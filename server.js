'use strict';

// Node libraries
const express = require('express');
var AsyncPolling = require('async-polling');
const https = require('https');

var log = [];
// log to console and capture log as data, so that in Docker it can be browsed or viewed in console
function dlog(logline) {
    console.log(logline);
    log.push(logline);
};


// Constants
const PORT = 8080;
// for local
// const HOST = 'localhost';
// for docker
const HOST = '0.0.0.0';

// Twilio set up, if wanting to text on thresholds
// const accountSid = 'process.env.TWILIO_ACCOUNT_SID';
// const authToken = 'process.env.TWILIO_AUTH_TOKEN';
// const client = require('twilio')(accountSid, authToken);

// App
const app = express();
app.get('/', (req, res) => {
  res.send(log);
});

app.listen(PORT, HOST);
dlog(`Running on http://${HOST}:${PORT}`);

class priceBot {

  constructor(threshold=0.0001, from_number = '+12015551111', to_number = '+12015552222'){
    this.threshold = threshold;
    this.baseline = [];
    this.from_number = from_number;
    this.to_mumber = to_number;
  }

  setBaseline(pair, ask, bid){
    dlog("Setting baseline prices for " + pair);
    this.baseline[pair] = [];
    this.baseline[pair]['ask'] = ask;
    this.baseline[pair]['bid'] = bid;
  }

  // threshold default assumes literal .01 percent, meaning .0001
  deltaPercentAlert(current, price_type, pair){
    this.alert = '...';
    if(current != 'undefined'){
      let threshold = this.threshold;
      let original = this.baseline[pair][price_type];
      let delta = Math.abs(original - current);
      if ( delta > 0 ) {
        dlog("Original " + price_type + " was: " + original);
        dlog("Current " + price_type + " is: " + current);
        dlog("Delta is: " + delta + "\n");
        let delta_pct = delta / original;
        dlog("Delta percentage is " + (delta_pct * 100).toFixed(2) + " %");
        this.alert = "Delta percentage does not meet threshold: " + delta_pct.toFixed(4);
        if ( delta_pct > threshold ) {
          this.alert = "==ALERT!== Delta of " + (delta_pct * 100).toFixed(2) + " % exceeds threshold of " + (threshold * 100).toFixed(2) + " %\n";
          // If this price alert should go to a person, enable if Twilio set up
          // this.sendAlert();
        }    
      }
    }
    return this.alert;
  }

  sendAlert(from_number, to_number){
    // Example send Alert with Twilio
    client.messages
      .create({
        body: this.alert,
        from: from_number,
        to: to_number
      })
      .then(message => dlog('Twilio message id: ' + message.sid));
  }

}


////////////////////////////
// Begin polling loop
////////////////////////////
let poll_interval = 5; // in seconds
let num_iterations = 300;
let pb = new priceBot();
let currency_pairs = ["BTC-USD", "BTC-EUR"];

currency_pairs.forEach( function(value) {
  let i = 0;
  var polling = AsyncPolling(function (end) {
    ++i;
      https.get('https://api.uphold.com/v0/ticker/'+value, (response) => {
        let resp_json = '';
      
        // called when a data chunk is received.
        response.on('data', (chunk) => {
          resp_json += chunk;
        });
      
        // called when the complete response is received.
        response.on('end', () => {
          // calling "bid" the sell price as in what you could sell your bitcoin for
          let current_bid = JSON.parse(resp_json).bid;
          // calling "ask" the buy price as in what you could currently buy bitcoin for
          let current_ask = JSON.parse(resp_json).ask;

          // if this is the first run, set the baseline prices
          if ( i === 1 ) {
            dlog("First iteration, setting baseline prices");
            pb.setBaseline(value, current_ask, current_bid);
          } else {
            // log the bid and ask values
            dlog("===========================");
            dlog(pb.deltaPercentAlert(current_bid, 'bid', value));
            dlog(pb.deltaPercentAlert(current_ask, 'ask', value));
          }
          dlog("Currency: " + JSON.parse(resp_json).currency);

        });  
    }).on("error", (error) => {
      dlog("Error: " + error.message);
    });

    if ( i >= num_iterations ) {
        this.stop();
        return end(null, '#' + i + ' stop');
    }
    end(null, '#' + i + ' waiting ...');
  }, poll_interval*1000);

  ['run', 'start', 'end', 'schedule', 'stop'].forEach(function (eventName) {
    polling.on(eventName, function () {
        // for tracing events
        // dlog('lifecycle:', eventName);
    });
  });

  polling.on('result', function (result) {
    // for debugging results
    // dlog('result:', result);
  });

  polling.on('error', function (error) {
    console.error('error:', error);
  });

  if (typeof(require) !== 'undefined') {
    polling.run();
  }
});

////////////////////////////
// End polling loop
////////////////////////////

