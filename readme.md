# Price Alert Bot

Price alert bot for Uphold ticker 

## Installation

```bash
git clone git@bitbucket.org:mhelenek/bot.git
cd bot
npm install
```

## Usage

### Docker 
(Change port forward if required for your env)

```bash
docker build . -t <username/botname>
docker run -p 8080:8080 -d <username>/botname>
```

Docker testing

```bash
docker ps
docker logs <container-id>
```

If using Docker Desktop, browsing to http://localhost:8080 will also show logs in browser


### Local Run

First change server.js to use localhost like below

```javascript
const HOST = 'localhost';
// for docker
// const HOST = '0.0.0.0';
```

Then run 

```bash
node server.js
```

## License
[MIT](https://choosealicense.com/licenses/mit/)